package technology.hunters.telegramblocked.component;

import org.springframework.stereotype.Component;
import technology.hunters.telegramblocked.tuple.TelegramState;

import java.util.HashSet;
import java.util.concurrent.atomic.AtomicReference;

@Component
public class Storage {
    private final AtomicReference<TelegramState> state = new AtomicReference<>(TelegramState.UNDEFINED);

    private final HashSet<Long> chatSubscriptions = new HashSet<>();

    private final AtomicReference<Long> lastTimeStamp = new AtomicReference<>(-1L);

    public void setState(TelegramState state) {
        this.state.set(state);
    }

    public TelegramState getState() {
        return this.state.get();
    }

    public void chatSubscriptionAdd(Long chatId) {
        chatSubscriptions.add(chatId);
    }

    public void chatSubscriptionRemove(Long chatId) {
        chatSubscriptions.remove(chatId);
    }

    public HashSet<Long> getChatSubscriptions() {
        return chatSubscriptions;
    }

    public Long getLastTimeStamp() {
        return lastTimeStamp.get();
    }

    public void setLastTimeStart(Long timeStamp) {
        lastTimeStamp.set(timeStamp);
    }
}
