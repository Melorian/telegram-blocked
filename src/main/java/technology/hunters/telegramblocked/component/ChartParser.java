package technology.hunters.telegramblocked.component;

import com.google.common.base.MoreObjects;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import technology.hunters.telegramblocked.tuple.Point;
import technology.hunters.telegramblocked.tuple.TelegramState;
import technology.hunters.telegramblocked.tuple.event.BlockedDiffEvent;

import java.util.List;

@Component
public class ChartParser {

    private static final Logger LOG = LoggerFactory.getLogger(ChartParser.class);

    private final ApplicationEventPublisher applicationEventPublisher;

    @Autowired
    public ChartParser(ApplicationEventPublisher applicationEventPublisher) {
        this.applicationEventPublisher = applicationEventPublisher;
    }

    public TelegramState parse(List<Point> chart) {
        if (chart == null || chart.isEmpty()) return TelegramState.UNDEFINED;

        Point[] lastTwoPoints = new Point[2];
        for (Point p: chart) {
            lastTwoPoints[0] = lastTwoPoints[1];
            lastTwoPoints[1] = p;
        }

        Point previousPoint = MoreObjects.firstNonNull(lastTwoPoints[0], lastTwoPoints[1]);
        Point currentPoint = lastTwoPoints[1];

        if (currentPoint != null) {
            long currentNrOfIps = currentPoint.y;
            long previousNrOfIps = previousPoint.y;

            if (currentNrOfIps - previousNrOfIps != 0) {
                BlockedDiffEvent event = new BlockedDiffEvent(this, previousNrOfIps, currentNrOfIps, currentPoint.y);
                applicationEventPublisher.publishEvent(event);
            }

            LOG.info("Prev IPs count: {}, cur IPs count: {}", previousNrOfIps, currentNrOfIps);
            return new TelegramState(currentNrOfIps, currentNrOfIps - previousNrOfIps);
        }

        return TelegramState.UNDEFINED;
    }
}
