package technology.hunters.telegramblocked.component;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import technology.hunters.telegramblocked.tuple.Point;
import technology.hunters.telegramblocked.tuple.TelegramState;

import java.net.URL;
import java.util.List;

@Component
public class Scheduler {

    private final Storage storage;
    private final ChartParser parser;

    @Autowired
    public Scheduler(Storage storage, ChartParser parser) {
        this.storage = storage;
        this.parser = parser;
    }

    private final Logger LOG = LoggerFactory.getLogger(Scheduler.class);

    private final ObjectMapper mapper = new ObjectMapper();
    private final String url = "https://usher2.club/d1_ipblock.json";

    @Scheduled(initialDelay = 5 * 1_000, fixedRate = 60 * 1_000)
    public void loadInfo() {
        LOG.info("Loading information...");

        try {
            URL address = new URL(url);
            List<Point> chart = mapper.readValue(address, new TypeReference<List<Point>>(){});
            TelegramState state = parser.parse(chart);
            storage.setState(state);
        } catch (Exception e) {
            LOG.info(String.format("Failed to load: %s", e.getMessage()));
        }

        LOG.info("Loading finished!");
    }
}
