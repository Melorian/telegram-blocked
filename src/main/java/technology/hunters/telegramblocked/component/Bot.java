package technology.hunters.telegramblocked.component;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.api.methods.groupadministration.GetChatMember;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.ChatMember;
import org.telegram.telegrambots.api.objects.MemberStatus;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.exceptions.TelegramApiException;
import technology.hunters.telegramblocked.component.commands.AmountCommand;
import technology.hunters.telegramblocked.component.commands.BotCommand;
import technology.hunters.telegramblocked.component.commands.UnwatchCommand;
import technology.hunters.telegramblocked.component.commands.WatchCommand;

import java.util.HashMap;

@Component
public class Bot extends TelegramLongPollingBot {

    private static final Logger logger = LoggerFactory.getLogger(Bot.class);

    @Value("${bot.token}")
    private String token;

    @Value("${bot.username}")
    private String username;

    private final HashMap<String, BotCommand> commandRegistry = new HashMap<>();

    @Autowired
    public Bot(AmountCommand amountCommand, WatchCommand watchCommand, UnwatchCommand unwatchCommand) {
        registerCommand("amount", amountCommand);
        registerCommand("watch", watchCommand);
        registerCommand("unwatch", unwatchCommand);
    }

    @Override
    public String getBotToken() {
        return token;
    }

    @Override
    public String getBotUsername() {
        return username;
    }

    @Override
    public void onUpdateReceived(Update update) {

        logger.info("Message received, processing...");

        if (update.hasMessage()) {
            Message message = update.getMessage();

            if (message.isCommand()) {
                String text = message.getText();
                BotCommand command = parseCommand(text);

                if (command != null) {
                    boolean isMessageFromAdmin = isMessageFromAdmin(message);

                    if (!command.isAdminOnly() || isMessageFromAdmin) {
                        String responseMessage = command.execute(message);

                        if (responseMessage != null) {
                            sendMessage(message.getChatId(), responseMessage);
                        } else {
                            logger.info("No command response, finishing!");
                        }
                    } else {
                        sendMessage(message.getChatId(), "Команда доступна только администратору");
                        logger.info("Command access restricted for admins only");
                    }
                } else {
                    sendMessage(message.getChatId(), "Команда не найдена");
                    logger.info(String.format("Command %s not found, skipping...", text));
                }
            } else {
                logger.info("Message is not command, skipping...");
            }
        } else {
            logger.info("Message is empty, skipping...");
        }

        logger.info("Message processed!");
    }

    private boolean registerCommand(String command, BotCommand handler) {
        if (commandRegistry.containsKey(command)) {
            logger.info(String.format("Command %s already registered", command));
            return false;
        } else {
            commandRegistry.put(command, handler);
            logger.info(String.format("Registered new command %s", command));
            return true;
        }
    }

    private BotCommand parseCommand(String text) {
        String command = text
                .replace("@"+getBotUsername(), "")
                .replace("/", "");

        return commandRegistry.getOrDefault(command, null);
    }

    private boolean isMessageFromAdmin(Message message) {
        GetChatMember request = new GetChatMember();
        request.setChatId(message.getChatId());
        request.setUserId(message.getFrom().getId());

        try {
            ChatMember member = this.getChatMember(request);
            return member.getStatus().equals(MemberStatus.CREATOR) || member.getStatus().equals(MemberStatus.ADMINISTRATOR);
        } catch (Exception e) {
            logger.info(String.format("Failed to load chat %s user %s role", request.getChatId(), request.getUserId()));
            return false;
        }
    }

    public void sendMessage(Long chatId, String message) {
        SendMessage response = new SendMessage();
        response.setChatId(chatId);
        response.setText(message);
        logger.info(String.format("Answering to chat %s with message: %s", response.getChatId(), response.getText()));
        try {
            execute(response);
            logger.info("Successfully answered!");
        } catch (TelegramApiException e) {
            logger.info(String.format("Failed to answer: %s", e.getMessage()));
        }
    }
}
