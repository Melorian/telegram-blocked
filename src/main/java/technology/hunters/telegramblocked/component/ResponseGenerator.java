package technology.hunters.telegramblocked.component;

import org.springframework.stereotype.Component;
import technology.hunters.telegramblocked.tuple.TelegramState;

@Component
public class ResponseGenerator {

    public String getIpsBlockedResponse(TelegramState state) {
        if (state.diff != 0) {
            char sign = state.diff > 0 ? '+': '-';
            return String.format("На текущий момент заблокировано адресов: %s (%c%d)", state.nrOfIpsBlocked, sign, Math.abs(state.diff));
        }
        return String.format("На текущий момент заблокировано адресов: %s", state.nrOfIpsBlocked);
    }

    public String getIpsBlockedChangeResponse(TelegramState state) {
        String change = state.diff > 0 ? "Добавлено": "Удалено";
        return String.format("%s адресов: %s, всего на текущий момент: %s", change, Math.abs(state.diff), state.nrOfIpsBlocked);
    }
}
