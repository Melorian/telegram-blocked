package technology.hunters.telegramblocked.component.commands;

import org.telegram.telegrambots.api.objects.Message;

import java.util.Optional;

public interface BotCommand {

    boolean isAdminOnly();

    String execute(Message message);
}
