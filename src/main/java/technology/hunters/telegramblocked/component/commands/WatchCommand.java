package technology.hunters.telegramblocked.component.commands;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.api.objects.Message;
import technology.hunters.telegramblocked.component.Storage;

@Component
public class WatchCommand implements BotCommand {

    private final Storage storage;

    @Autowired
    public WatchCommand(Storage storage) {
        this.storage = storage;
    }

    @Override
    public boolean isAdminOnly() {
        return true;
    }

    @Override
    public String execute(Message message) {
        storage.chatSubscriptionAdd(message.getChatId());
        return "Отслеживание изменения включено";
    }
}
