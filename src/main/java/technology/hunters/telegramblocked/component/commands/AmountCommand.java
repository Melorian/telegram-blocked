package technology.hunters.telegramblocked.component.commands;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.api.objects.Message;
import technology.hunters.telegramblocked.component.ResponseGenerator;
import technology.hunters.telegramblocked.component.Storage;

@Component
public class AmountCommand implements BotCommand {

    private final Storage storage;
    private final ResponseGenerator responseGenerator;

    @Autowired
    public AmountCommand(Storage storage,
                         ResponseGenerator responseGenerator) {
        this.storage = storage;
        this.responseGenerator = responseGenerator;
    }

    @Override
    public boolean isAdminOnly() {
        return false;
    }

    @Override
    public String execute(Message message) {
        return responseGenerator.getIpsBlockedResponse(storage.getState());
    }
}
