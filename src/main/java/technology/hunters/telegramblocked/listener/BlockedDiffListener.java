package technology.hunters.telegramblocked.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import technology.hunters.telegramblocked.component.Bot;
import technology.hunters.telegramblocked.component.ResponseGenerator;
import technology.hunters.telegramblocked.component.Storage;
import technology.hunters.telegramblocked.tuple.event.BlockedDiffEvent;

@Component
public class BlockedDiffListener implements ApplicationListener<BlockedDiffEvent> {

    private final Logger LOG = LoggerFactory.getLogger(BlockedDiffListener.class);

    private final Bot bot;
    private final Storage storage;
    private final ResponseGenerator responseGenerator;

    @Autowired
    public BlockedDiffListener(Bot bot,
                               Storage storage,
                               ResponseGenerator responseGenerator) {
        this.bot = bot;
        this.storage = storage;
        this.responseGenerator = responseGenerator;
    }

    @Override
    public void onApplicationEvent(BlockedDiffEvent event) {
        if (storage.getLastTimeStamp() < event.getLastTimeStamp()) {
            LOG.info("Updated diff, notifying...");
            storage.setLastTimeStart(event.getLastTimeStamp());
            storage.getChatSubscriptions().forEach(chatId -> {
                String message = responseGenerator.getIpsBlockedChangeResponse(storage.getState());
                bot.sendMessage(chatId, message);
            });
            LOG.info("Diff notification sent!");
        } else {
            LOG.info("Diff is the same, no need to notify");
        }
    }
}