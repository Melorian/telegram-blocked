package technology.hunters.telegramblocked.tuple;

import java.util.Objects;

public class TelegramState {

    public static final TelegramState UNDEFINED = new TelegramState(-1, 0);

    public TelegramState(long nrOfIpsBlocked, long diff) {
        this.nrOfIpsBlocked = nrOfIpsBlocked;
        this.diff = diff;
    }

    public final long nrOfIpsBlocked;
    public final long diff;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TelegramState that = (TelegramState) o;
        return nrOfIpsBlocked == that.nrOfIpsBlocked &&
                diff == that.diff;
    }

    @Override
    public int hashCode() {
        return Objects.hash(nrOfIpsBlocked, diff);
    }
}
