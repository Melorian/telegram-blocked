package technology.hunters.telegramblocked.tuple;

public class Point {
    public Point() {
    }

    public Point(long x, long y) {
        this.x = x;
        this.y = y;
    }

    public long x;
    public long y;
}
