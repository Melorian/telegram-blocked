package technology.hunters.telegramblocked.tuple.event;

import org.springframework.context.ApplicationEvent;

public class BlockedDiffEvent extends ApplicationEvent {

    private Long previous;
    private Long current;
    private Long lastTimeStamp;

    public BlockedDiffEvent(Object source, Long previous, Long current, Long lastTimeStamp) {
        super(source);
        this.previous = previous;
        this.current = current;
        this.lastTimeStamp = lastTimeStamp;
    }

    public Long getPrevious() {
        return previous;
    }

    public Long getCurrent() {
        return current;
    }

    public Long getLastTimeStamp() {
        return lastTimeStamp;
    }
}