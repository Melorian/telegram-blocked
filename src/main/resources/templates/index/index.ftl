<#import "../layout/outer.ftl" as layout>

<@layout.render>

    <!-- start preloader -->
    <div class="preloader">
        <div class="sk-spinner sk-spinner-wave">
            <div class="sk-rect1"></div>
            <div class="sk-rect2"></div>
            <div class="sk-rect3"></div>
            <div class="sk-rect4"></div>
            <div class="sk-rect5"></div>
        </div>
    </div>
    <!-- end preloader -->

    <!-- start navigation -->
    <nav class="navbar navbar-default templatemo-nav" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon icon-bar"></span>
                    <span class="icon icon-bar"></span>
                    <span class="icon icon-bar"></span>
                </button>
                <a href="/" class="navbar-brand">Блокировки Telegram</a>
            </div>
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="/">Главная</a></li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- end navigation -->

    <!-- start home -->
    <section id="home">
        <div class="container">
            <div class="row">
                <div class="col-md-offset-2 col-md-8">
                    <h1 class="wow fadeIn" data-wow-offset="50" data-wow-delay="0.9s">Заблокировано IP:<br>
                        <span>${ips}<#if diff gt 0> (+${diff})<#elseif diff lt 0> (-${diff})<#else></#if></span></h1>
                    <div class="element">
                        <div class="sub-element">Но всем пофиг</div>
                        <div class="sub-element">Все желающие уже настроили VPN</div>
                        <div class="sub-element">Миллион запасных IP адресов уже готов</div>
                        <div class="sub-element">А РКН по прежнему сосет</div>
                        <div class="sub-element">¯\_(ツ)_/¯</div>
                    </div>
                    <a data-scroll href="#" class="btn btn-default wow fadeInUp" data-wow-offset="50" data-wow-delay="0.6s">НИЧЕГО НЕ ДЕЛАТЬ</a>
                </div>
            </div>
        </div>
    </section>
    <!-- end home -->

    <!-- start about -->
    <section id="about">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="wow bounceIn" data-wow-offset="50" data-wow-delay="0.3s">ПОЧЕМУ НАМ <span>НЕ СТРАШНО</span>?</h2>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12 wow fadeInLeft" data-wow-offset="50" data-wow-delay="0.6s">
                    <div class="media">
                        <div class="media-heading-wrapper">
                            <div class="media-object pull-left">
                                <i class="fa fa-mobile"></i>
                            </div>
                            <h3 class="media-heading">ВСЕМ ПОФИГ</h3>
                        </div>
                        <div class="media-body">
                            <p>Серьезно. Кто хочет пользоваться Телеграмом - сможет разобраться и настроить VPN, а так же научиться менять его в течение пяти минут.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12 wow fadeInUp" data-wow-offset="50" data-wow-delay="0.9s">
                    <div class="media">
                        <div class="media-heading-wrapper">
                            <div class="media-object pull-left">
                                <i class="fa fa-comment-o"></i>
                            </div>
                            <h3 class="media-heading">ПАША РАЗБЕРЕТСЯ</h3>
                        </div>
                        <div class="media-body">
                            <p>И снова серьезно. Дуров дядька умный, а работают на него дядьки еще умнее. И деньги у него есть, да - сможет купить еще несколько миллионов IP адресов</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12 wow fadeInRight" data-wow-offset="50" data-wow-delay="0.6s">
                    <div class="media">
                        <div class="media-heading-wrapper">
                            <div class="media-object pull-left">
                                <i class="fa fa-html5"></i>
                            </div>
                            <h3 class="media-heading">РКН СОСЕТ</h3>
                        </div>
                        <div class="media-body">
                            <p>Просто сосет. Без причины. Сами не знаем, почему.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- end about -->

    <!-- start copyright -->
    <footer id="copyright">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <p class="wow bounceIn" data-wow-offset="50" data-wow-delay="0.3s">
                        Copyright &copy; 2018 Hungry hamsters</p>
                </div>
            </div>
        </div>
    </footer>
    <!-- end copyright -->

</@layout.render>