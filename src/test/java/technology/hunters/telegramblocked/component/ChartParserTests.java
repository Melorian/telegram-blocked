package technology.hunters.telegramblocked.component;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationEventPublisher;
import technology.hunters.telegramblocked.tuple.Point;
import technology.hunters.telegramblocked.tuple.TelegramState;

import java.util.Arrays;
import java.util.Collections;

import static org.junit.Assert.assertEquals;

@SpringBootTest
public class ChartParserTests {

    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    ChartParser parser = new ChartParser(applicationEventPublisher);

    @Test
    public void WhenChartIsEmpty_ThenReturnUndefined() {
        TelegramState state = parser.parse(Collections.emptyList());
        assertEquals(TelegramState.UNDEFINED, state);
    }

    @Test
    public void WhenChartConsistsOfSingleElement_ThenDiffIs0() {
        Point p = new Point(1, 2);

        TelegramState state = parser.parse(Collections.singletonList(p));

        assertEquals(new TelegramState(p.y, 0), state);
    }

    @Test
    public void WhenChartConsistsManyElement_ThenDiffIsCorrect() {
        Point prev = new Point(1, 2);
        Point last = new Point(2, 5);

        TelegramState state = parser.parse(Arrays.asList(prev, last));

        assertEquals(new TelegramState(last.y, last.y - prev.y), state);
    }
}