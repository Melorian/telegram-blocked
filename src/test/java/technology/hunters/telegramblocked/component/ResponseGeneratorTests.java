package technology.hunters.telegramblocked.component;

import org.junit.Test;
import technology.hunters.telegramblocked.tuple.TelegramState;

import static org.junit.Assert.assertEquals;

public class ResponseGeneratorTests {
    private final ResponseGenerator rg = new ResponseGenerator();

    @Test
    public void WhenDiffPositive_ThenReturnProperMessage() {
        TelegramState state = new TelegramState(10, 5);
        String response = rg.getIpsBlockedResponse(state);

        assertEquals("На текущий момент заблокировано адресов: 10 (+5)", response);
    }

    @Test
    public void WhenDiffNegative_ThenReturnProperMessage() {
        TelegramState state = new TelegramState(10, -5);
        String response = rg.getIpsBlockedResponse(state);

        assertEquals("На текущий момент заблокировано адресов: 10 (-5)", response);
    }

    @Test
    public void WhenDiffIs0_ThenReturnProperMessage() {
        TelegramState state = new TelegramState(10, 0);
        String response = rg.getIpsBlockedResponse(state);

        assertEquals("На текущий момент заблокировано адресов: 10", response);
    }
}
